#
# spec file for package librime
#
# Copyright (c) 2017 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#
%define rime_version 1.5.3

Name:           librime
Version:        %{rime_version}
Release:        0
Summary:        Rime Input Method Engine
License:        BSD-3-Clause
Group:          System/I18n/Chinese
Url:            https://github.com/rime/librime
Source:         %{name}-%{version}.tar.xz
BuildRequires:  boost-devel
BuildRequires:  boost-static
BuildRequires:  cmake
BuildRequires:  gcc-c++
BuildRequires:  glibc-common


BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
Rime is an Traditional Chinese input method engine.
Its idea comes from ancient Chinese brush and carving art.
Mainly it's about to express your thinking with your keystrokes.
This package is the runtime libraries of Rime.

%package devel
Summary:        Development files of Rime
Group:          Development/Libraries/C and C++
Requires:       librime = %{version}

%description devel
Rime is an Traditional Chinese input method engine.
Its idea comes from ancient Chinese brush and carving art.
Mainly it's about to express your thinking with your keystrokes.

This package is the development headers of Rime.

%prep
%setup -q -n %{name}-%{version}/%{name}

%build
make %{?_smp_mflags} thirdparty
make %{?_smp_mflags} librime-static 
make %{?_smp_mflags} merged-plugins


%install
make %{?_smp_mflags} DESTDIR=%{buildroot} install-static
cp -a src/rime %{buildroot}/usr/include/


%post -n librime -p /sbin/ldconfig
%postun -n librime -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc LICENSE README.md
%{_libdir}/%{name}.a

%files devel
%defattr(-,root,root)
%{_includedir}/rime_api.h
%{_includedir}/rime_levers_api.h
%{_includedir}/rime/
%{_libdir}/pkgconfig/rime.pc
%{_datadir}/cmake/rime/

%changelog
